from hypothesis import given, strategies as st
import hypothesis
import hypothesis.extra.pandas as hp
import preprocessing


@given(
        hp.data_frames(
            columns=[
                hp.column("Price", elements=st.integers(min_value=1000, max_value=5000)), 
                hp.column("Brick", elements=st.one_of(st.none(), st.just("yes"), st.just("no"))), 
                hp.column("Neighborhood", elements=st.one_of(st.none(), st.just("East"), st.just("North"), st.just("West")))
            ]
        )
)
def test_preprocessing(df):
    #hypothesis.assume(not df.empty)
    hypothesis.assume(df.shape[0]>1)
    X_train, X_valid, y_train, y_valid = preprocessing.preprocessing(df)
    assert X_train.shape[0]>=X_valid.shape[0]
    assert (X_train['Brick'] == 0 or 1).all()
