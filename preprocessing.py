import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OrdinalEncoder

data = pd.read_csv('data/preprocessed/house-prices.csv')

def preprocessing(data):
    
    # Отеделение цели от признаков
    y = data.Price
    X = data.drop(['Price'], axis=1)

    # Деление на тестовые и тренировочные сеты
    X_train_full, X_valid_full, y_train, y_valid = train_test_split(X, y, train_size=0.8, test_size=0.2,
                                                                    random_state=0)

    # Удаление пустых столбцов
    cols_with_missing = [col for col in X_train_full.columns if X_train_full[col].isnull().any()] 
    X_train_full.drop(cols_with_missing, axis=1, inplace=True)
    X_valid_full.drop(cols_with_missing, axis=1, inplace=True)

    # выбор столбцов, которые можно легко закодировать
    low_cardinality_cols = [cname for cname in X_train_full.columns if X_train_full[cname].nunique() < 10 and 
                            X_train_full[cname].dtype == "object"]

    # выбор числовых столбцов
    numerical_cols = [cname for cname in X_train_full.columns if X_train_full[cname].dtype in ['int64', 'float64']]

    # Keep selected columns only
    my_cols = low_cardinality_cols + numerical_cols
    X_train = X_train_full[my_cols].copy()
    X_valid = X_valid_full[my_cols].copy()

    # Получение категориальных признаков
    s = (X_train.dtypes == 'object')
    object_cols = list(s[s].index)

    # Применим Ordinal Encoder к нашим категориальным данным
    ordinal_encoder = OrdinalEncoder()
    X_train[object_cols] = ordinal_encoder.fit_transform(X_train[object_cols])
    X_valid[object_cols] = ordinal_encoder.transform(X_valid[object_cols])
    print(X_train.shape[0])
    print(X_valid.shape[0])
    return X_train, X_valid, y_train, y_valid
preprocessing(data)